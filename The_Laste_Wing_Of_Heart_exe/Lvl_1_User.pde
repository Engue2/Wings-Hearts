///// Perso
//////////////////////////////////////////////////////////////////
class Perso implements Entity {
  PVector position=new PVector();
  PVector velocity=new PVector();
  PVector focus= new PVector();
  // PVector acceleration = new PVector(); // velocity.y += acceleration.y; velocity.x += acceleration.x;
  
  PImage sprite;
  float sizeW;
  float sizeH;
  
  boolean inverser = false;
  // AABB
  int val=0;

  // Search max width and height of the ship.
  float posMaxWidth;
  float posMaxHeight;
  // Search Midel the ship points.
  float posMidelWidth;
  float posMidelHeight;
  Perso(PImage sprite,float sizeW, float sizeH) {
    this.sprite=sprite;
    this.sizeW=sizeW;
    this.sizeH=sizeH;
    position.x=width * .5;
    position.y=height * .5;
    
    posMaxWidth = width - sizeW;
    posMaxHeight = height - sizeH;   
    posMidelHeight = sizeH / 2;
    posMidelWidth = sizeW / 2;
  }
  void draw() {
    position.y+=velocity.y;
    position.x+=velocity.x;
    focus.x = position.x + posMidelWidth; focus.y = position.y + posMidelHeight;
    LimiteOfMoveSetUser();
    
    fill(25, 4, 239);
    HitTheNpc();
    image(sprite, position.x, position.y);
  }
  
  void HitTheNpc(){
    if(val > 0 ){
      if(val % 2 == 0){
      sprite.filter(INVERT);
      inverser = !inverser;
      }
      val--;
    }else if(inverser == true){
      inverser = false;
      sprite.filter(INVERT);
    }
  }
  
  void openType(int speedShip){
    if(keyCode == DOWN){ velocity.y = speedShip; } 
    if(keyCode == UP){ velocity.y = -speedShip; }
    if(keyCode == LEFT){ velocity.x = -speedShip; } 
    if(keyCode == RIGHT){ velocity.x = speedShip; } 
  }
  void endType(){ 
    if(keyCode == DOWN){ velocity.y = 0; } 
    if(keyCode == UP){ velocity.y = 0; }
    if(keyCode == LEFT){ velocity.x = 0; } 
    if(keyCode == RIGHT){ velocity.x = 0; } 
  }
  void LimiteOfMoveSetUser() {
    if (position.x < 0) {
      position.x = 0;
    }
    if (position.x > posMaxWidth) {
      position.x = posMaxWidth;
    }
    if (position.y < 0) {
      position.y = 0;
    }
    if (position.y > posMaxHeight) {
      position.y = posMaxHeight;
    }
  }
}

class Shot_User_In_Game extends Perso implements Entity {
  PVector position=new PVector();
  PVector velocity=new PVector();
  float sizeH;
  float sizeW;
  float index=0;
  Layer layer;
  Timer a = new Timer();
  
  Shot_User_In_Game(PImage Sprite,float sizeW, float sizeH,Layer layer,float pos_x,float pos_y,float speed_bullet) {
    super(Sprite,sizeW,sizeH);
    this.layer=layer;
    this.sizeW=sizeW;
    this.sizeH=sizeH;
    position.x=pos_x;
    position.y=pos_y;
    velocity.y=speed_bullet;
  }
  void draw() {
    LimiteOfBound();
    
    position.y+=-velocity.y;
    
    fill(255, 0, 0);
    rect(position.x,position.y,sizeW,sizeH);
  }
  void LimiteOfBound(){
   if(position.y < 0){
    layer.removeEntity(this);
   }
  }
}