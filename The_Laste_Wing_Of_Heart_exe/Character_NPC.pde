// Les mecanique reste les même que pour 'Character' on garde les même mécanique.
class Charater_NPC_Game_Meta {
  // type Of Ship;
  String name_Ship;
  // Disigne ship
  PImage ship_disigne;
  // Nombre de HP .
  int charater_HP;
  // Nombre de bouclier .
  int charater_Shield;
  // Vitesse .
  int charater_Speed;
  // Taile .
  int charater_Size;
  
  Charater_NPC_Game_Meta(String type,PImage sprite){
    this.name_Ship=type;
    this.ship_disigne=sprite;
  }
  
  void TypeOfNpcShip(){
     if(name_Ship == "Std_NPC_type_01"){
      // Sprite STD NPC ship_disigne = loadImage("Sprite/Type_1_Sprite_User.png");
      charater_HP=25;             
      charater_Shield=3;
      charater_Speed=3;
      charater_Size=10;
     }
     if(name_Ship == "Std_NPC_type_02"){
      charater_HP=50;             
      charater_Shield=5;
      charater_Speed=5;
      charater_Size=20;
     }
     if(name_Ship == "Std_NPC_type_03"){
      charater_HP=100;             
      charater_Shield=20;
      charater_Speed=7;
      charater_Size=43;
     }
  }
}

class NPC_BarreDeVie {
  int NPC_hp_max;
  float NPC_hp;
  float NPC_hp_Active;
  
  float NPC_degat_all;
  boolean NPC_Active_domage = false;
  int index=0;
  NPC_BarreDeVie(int NPC_hp_max_ship,int NPC_degat){
    NPC_hp_max = NPC_hp_max_ship;
    NPC_hp = NPC_hp_max;
    NPC_degat_all = NPC_degat;
  }
  
  void take_Domage(float degat_all,boolean Active_domage){
      // Quand le non playeur caracter subie des dégats
      // degat_all : c'est se que a subie en degat le npc
      // Active_Domage : permez de savoir si oui ou non il prends les dégats .
      if(Active_domage == false && index == 0){
          NPC_hp = NPC_hp - degat_all;
          Active_domage = true;
      }else {
          Active_domage = true;
      }
  }
}

class NPC_BarreShield {
  int NPC_shield_max;
  float NPC_shield;
  float NPC_riotActive;
  
  boolean NPC_Active_domage = true;
   
  NPC_BarreShield(int NPC_shield_max_game){
    NPC_shield_max = NPC_shield_max_game;
    NPC_shield = NPC_shield_max;
  }
  
  void take_Domage(float degat_all){
     // a savoir si le bouclier du on playeur caracter subie des dégats .
     // degats_all : dégat subie par le bouclier du npc
     NPC_shield = NPC_shield - degat_all;
    // println("Nombre de Point de Bouclier max"+NPC_shield_max+" Est Nombre de points de Vie acutelle "+);
  }
}