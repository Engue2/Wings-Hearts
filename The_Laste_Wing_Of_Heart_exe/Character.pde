enum Jambom { /*  Jambom.wapon.stat */
  /* Nous Affont la basse de donné des arme est de leur statistique. */
  /* Elle permez de initier des valeur de base des arme pour les stocker est les réutiliser. */
                        /*DEG/DC/SP/MA*/
  std_v1("Standar Wapon V1",5,0.5,5,5,0,0),
  std_v2("Standar Wapon V2",7,0.45,5.5,5,25,25),
  std_v3("Standar Wapon V1",10.5,0.3,6,5,50,50),
  
  std_speed_fire_v1("Standar Speed Fire Wapon V1",2.5,0.25,7.5,2.5,0,0),
  std_speed_fire_v2("Standar Speed Fire Wapon V1",3.5,0.235,7.8,2.5,0,0),
  std_speed_fire_v3("Standar Speed Fire Wapon V1",5,0.2,8,2.5,0,100),
  
  std_bow_v1("Standar Bow Wapon V1",10,1.5,3,10,0,0),
  std_bow_v2("Standar Bow Wapon V2",13.5,1.2,3,10,0,0),
  std_bow_v3("Standar Bow Wapon V3",15,1,3,10,100,0),
  
  S_railgun_V1("Railgun Rank S Wapon",30,4,10,1.5,0,50),
  SS_railgun_V2("Railgun Rank SS Wapon",45,3.5,10,1.5,100,0),
  SSS_railgun_V3("Railgun Rank SSS Wapon",60,3,10,2,200,0);
  
  private String name = "";
  private float degat = 0;
  private float cd = 0;
  private float speed;
  private float mass;
  private float BonusDomageWhitShield = 0;
  private float BonusDimagWhitePv = 0;
  private Jambom(String Name,float DEG,float DC,float SP,float MA,float BDInShield,float BDInPv){
    name = Name;
    degat = DEG;
    cd = DC;
    speed = SP;
    mass = MA;
    BonusDomageWhitShield = BDInShield;
    BonusDimagWhitePv = BDInPv;
  }
}
class Charater_Game_Meta{
  /* Nous Affont la basse de donné des vaiseau est de leur statistique pouvent étre utiliser par le joueur.  */
  /* Elle permez de initier des valeur de base des vaiseau pour les stocker est les réutiliser. */
  /* Example des valeur sortis : */
  // Nom du vaiseau : "Un Super Ship" 
  // L'image du sprite du vaiseau : "limagedeonvaiseau.png"
  // Nombre de points de Vie : "3"
  // Nombre de points de bouclier : "333"
  // Nombre de la vitesse de points de vie régeneres : = "300" (300 seconde)
  // Nombre de points de vie régenres = "1"
  // Nombre de Mana : "60"
  // Vitesse du vaiseau : "5" (velocity x : 5 , velocoty y : 5)
  // Taille du vaiseau : "43" (43x43)
  
  // Brute Stat : type Of Ship;
  // Nom : Sprite : HP : Shield : Speed Régen : Hit Regne : Vitesse : Taille :
  
  String name_Ship;
  PImage ship_disigne;
  int charater_HP;
  int charater_Shield;
  int charater_Speed_Regen_S;
  float charater_Regen_S;
  int charater_Mana;
  int charater_Speed;
  int charater_Size;
  
  Charater_Game_Meta(String type,PImage sprite){
    this.name_Ship=type;
    this.ship_disigne=sprite;
  }
   
   void TypeOfShip(){
     /* mecanique pour definir le vaiseau du joueur.*/
     if(name_Ship == "Type_1"){
      ship_disigne = loadImage("Sprite/Type_1_Sprite_User.png");
      charater_HP=200;charater_Shield=20;charater_Regen_S=2;charater_Speed_Regen_S=100;charater_Mana=25;charater_Speed=7;charater_Size=43;
     }
     if(name_Ship == "Soldat"){
      ship_disigne = loadImage("Sprite/Soldat_1_Sprite_User.png");
      charater_HP=300;charater_Shield=100;charater_Regen_S=1.5;charater_Speed_Regen_S=300;charater_Mana=10;charater_Speed=6;charater_Size=46;               
     }
     if(name_Ship == "Ghost"){
      ship_disigne = loadImage("Sprite/Ghost_1_Sprite_User.png");
      charater_HP=50;charater_Shield=0;charater_Regen_S=0;charater_Speed_Regen_S=0;charater_Mana=40;charater_Speed=9;charater_Size=36;
     }
     if(name_Ship == "Ingé"){
      ship_disigne = loadImage("Sprite/Ingé_1_Sprite_User.png");
      charater_HP=800;charater_Shield=0;charater_Regen_S=0;charater_Speed_Regen_S=0;charater_Mana=40;charater_Speed=4;charater_Size=60;            
     }
   } 
}

class BarreDeVie {
  /* BarreDeVie lier aux joueur. */
  /* mecanique de PV + mecanique d'affichage . */
  // Valeur Example HP :
  // hp_max : Nombre de points de vie maximume du vaiseau
  // hp_user : Points de vie actuelle du joueur.
  // pvActive : Est égale au nombre de pv réelle.+
  // pvPourcent : Le nombre de pv en pourcentage (%)
  // degat_all : Le degat reçu.
  // Active_domage : Si oui ou non le joueur subira des dégats 
  // index : indice permetant de prevenir de bug/jouer avec les frame. 
  // sprite_active_shield_patherne : permez de savoir si oui ou non le bouclier du joueur est active .
  // myFont : valeur permetant de choisir la typo de la font & taille .
  // text_PV :  valeur permetant l'ecriture du text dans le 'currentscreen' 
  int hp_max;
  float hp_user;
  float pvActive;
  float pvPourcent;
  float degat_all;
  boolean Active_domage = false;
  int index=0;
  boolean sprite_active_shieald_patherne = true;
  PFont myFont;
  String text_PV;
  
  BarreDeVie(int hp_max_ship,int degat){
    hp_max=hp_max_ship;
    hp_user=hp_max;
    degat_all=degat;
   
  myFont = createFont("Corbel Bold", 20);
  textFont(myFont);
  }
  
  void draw(){
    calculeBarre();
    BarreHP();
  }
  
  void calculeBarre(){
    // mecanique pour crée un visuel sur le nombre de points de vie que posséed le joueur .
    if(hp_user < 0){
    // Le joueur est morts
      in_the_void_end();
      //currentScreen = new lvl1();
    }else{
      pvActive = hp_max - hp_user;
      pvActive = (pvActive / hp_max) * 100;
      
      pvPourcent = 100 - pvActive;
    }
  }
  
  void in_the_void_end(){
  /// EN CONSTRUCTION ///
  // Prévue pour implementer un system future de la mort du joueur.
  // index++;
  }
  
  void BarreHP(){
  // Fonction d'affichage des points de vie en pourcentage du joueur + l'animation d'une barre.
  text_PV = pvPourcent+"%";
  fill(255);
  stroke(0);
  rect(width-300,height-50,300,50);
  fill(255,0,0);
  rect(width-300,height-50,pvPourcent*3,50);
  fill(0);
  text(text_PV,width-150,height-25);
  }
  
  void take_Domage(float degat_all,boolean Active_domage){
  // fonction qui permez au joueur de subire des dégats .
  // degat_all = les dégats subie par le joueur.
  // Active_domage = permez de savoir si le joueur peux subire des dégats.
  
    if(Active_domage == false && index == 0){
      hp_user = hp_user - degat_all;
      Active_domage = true;
    }else {
      Active_domage = true;
    }
  
    if(Active_domage == true){
      index++;
      if(index > 1){
        index=0;
        Active_domage = false;
      }
    }
   
  }
  /// Fin de la Class BarreDeVie  ///
}
class BarreShield_User{
  /* BarreShield_User lier aux joueur. */
  /* mecanique de Bouclier + mecanique d'affichage . */
  // Valeur Example HP :
  // shield_max : Nombre de points de bouclier maximume du vaiseau
  // shield_user : Nombre de points de bouclier que le joueur posséde.
  // riotActive : 
  // riotPoucent : Le Nombre de points de bouclier en pourcentage.
  // Active_domage : Si oui ou non le joueur subira des dégats 
  // activeregen : Si oui ou non la regeneration de  bouclier est activer.
  // regencd : La vitesse que le bouclier mais a se règeneres 
  // ticregen : Le Nombre de points de bouclier récupere par regeneration.
  // csr_s : 
  // cr_s :
  // myFont : valeur permetant de choisir la typo de la font & taille .
  // text_PV :  valeur permetant l'ecriture du text dans le 'currentscreen'
  int shield_max;
  float shield_user;
  float riotActive;
  float riotPoucent;
  
  boolean Active_domage = true;
 
  boolean activeregen = false;
  float regencd;
  int ticregen;
  
  int csr_s;
  float cr_s;
  
  PFont myFont;
  String text_PV;
  BarreShield_User(int shield_max_in_game,float cr,int csr){
    shield_max=shield_max_in_game;
    shield_user=shield_max;
    
    regencd = 100;ticregen = 20;
    /* speed regen */csr_s = csr; /* regen */cr_s = cr;
    myFont = createFont("Corbel Bold", 20);
    textFont(myFont);
  }
  void draw(){
    calculeshield();
    barreshield();
  }
  void calculeshield(){
  // Si le joueur posséed un bouclier on entre dans la mecanique du bouclier.
    if(shield_max != 0){
      regen();
      
    if(shield_user < 0){
      shield_user=0;
    }
    // Le même calcule que pour calculer le pourcentage raistant de vie du joueur(Class BarreDeVie de la fonction calculeBarre() ). 
    riotActive = shield_max - shield_user;
    riotActive = (riotActive / shield_max) * 100;
     
    riotPoucent = 100 - riotActive;
    }else if(Active_domage == true){
      Active_domage = false;
    }else{
    // println("The Type of ship no used shield");
    }
   }
   
  void barreshield(){
  if(shield_max != 0){
    text_PV = riotPoucent+"%";
    fill(255);
    ellipse(width-160,height-160,160,160);
 
    fill(255,5,5);
    float res = (3.5 / 100) * riotPoucent;
    arc(width-160, height-160, 160, 160, 1.5,1.5+res);
  
    fill(0);
    text(text_PV,width-160,height-160);
   }
  }
  
  void take_Domage(float degat_all){
     regencd = 0;
     shield_user = shield_user - degat_all;
  }
  
  void regen(){
    // On crée les mécanique de regeneration des points de bouclier du joueur.
    regencd += 0.25;
    if(regencd > csr_s){
       activeregen = true;
       regencd = csr_s;
       ticregen += 1;
    }
    
    if(regencd == csr_s && activeregen == true && ticregen > 50){
       activeregen = false;
       Active_domage = true;
       ticregen = 0;
       
       shield_user = shield_user + cr_s;
       
       if(shield_user > shield_max){
         shield_user = shield_max;
       }
    }
  }
  
}