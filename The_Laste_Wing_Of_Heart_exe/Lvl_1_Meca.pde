////// Background Effect
//////////////////////////////////////////////////////////////////
class Star implements Entity {
  PVector position=new PVector();
  PVector velocity=new PVector();
  float size=random(3, 6);
  boolean test = false;
  Star() {
    position.x=random(0, width);
    position.y=random(0, height);
    velocity.y=random(3, 6);
  }
  void draw() {
    if (position.y > height) {
      position.y=0;
      position.x=random(0, width);
    }
    
    position.y+=velocity.y;
    fill(255);
    ellipse(position.x,position.y,size,size);
  }
}
//// Timer
////////////////////////////////////////////////////////////////////
class Timer {
  float time_appl=millis();
  float time_appl_seconde = time_appl * .001;

  float timer_In_Game;
  float timer_In_Game_Seconde;

  void draw() {
    getTimer();
  }
  void getTimer() {
    timer_In_Game = millis();
    timer_In_Game_Seconde = (timer_In_Game * .001) - time_appl_seconde;
  }
}