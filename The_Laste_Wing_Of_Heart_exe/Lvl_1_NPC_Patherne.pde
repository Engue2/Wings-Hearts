abstract class Path {
  int[] tab;
  void Trigger(Non_Playing_Characte e){
  
  }
}
/* m = rapide 
   l = lent
   Top Screen
   | 0 | 1 | 2 | 3 | 4 | 5 | 6 |
   |                           |
   | 0 | 1 | 2 | 3 | 4 | 5 | 6 |
   Bottom Screen
*/

class Path001 extends Path {
  int[] tab;
  int ind = 0;
  int decalage = 4;
  float timeradd = 0;
  Path001(int[] patherne){
    this.tab = patherne; 
  }
  void Trigger(Non_Playing_Characte e){
    for(int indice: e.tab){
      switch(e.tab[indice]){
      case 0:
        if(e.Time_Start > 1 && e.Time_Start < 3){
           
           e.position.x = 50;
           e.position.y = 50;
        }
      break;
      
      case 1:
        if(e.Time_Start > (4 + timeradd) && e.Time_Start < (6 + timeradd)){
          e.velocity.x = 3;
        }
      break;
      
      case 2:
       if(e.Time_Start > (6 + timeradd) && e.Time_Start < (8 + timeradd)){
        e.velocity.x = -3;
       }
      break;
      
      case 3:
         if(e.Time_Start > (8 + timeradd)){
           timeradd += int(e.Time_Start - decalage);
           decalage = decalage + 4;
           Trigger(e);
         }
      break;
      }
    }
  }
}
/* Path00m pare du  haut-gauche pour aller bas-gauche */

/* Path03m pare du  haut-gauche pour aller bas-milieux */
class Path03m extends Path {
  int[] tab;
  Path03m(int[] patherne){
    this.tab = patherne; 
  }
  void Trigger(Non_Playing_Characte e){
    for(int indice: e.tab){
           switch(e.tab[indice]){
           case 0:
             if(e.Time_Start > 1 && e.Time_Start < 4.5){
               e.position.x = 100;
               e.position.y = -80;
             }
           break;
           case 1:
             if(e.Time_Start > 4.5 && e.Time_Start < 5){ 
                 e.velocity.y = 3;
             }
           break;
           case 2:
             if(e.Time_Start > 5 && e.Time_Start < 7){     
                 e.velocity.y = 0;
             }
           break;
           case 3:
             if(e.Time_Start > 7 && e.Time_Start < 10){    
                 e.velocity.y = 4;
                 e.velocity.x = 2;
             }
             
           break;
           case 4:
              if(e.Time_Start > 15){
                 e.expireNpc = true;
              }
           break;
           }
      }
  }
}
/* Path06m pare du  haut-gauche pour aller bas-droit */

/* Path30m pare du  haut-millieux pour aller bas-gauche */

/* Path33l pare dut haut-millieux pour bas-millieux */
class Path33l extends Path{
  int[] tab;
  Path33l(int[] Patherne){
     this.tab=Patherne;
  }
  void Trigger(Non_Playing_Characte e){
     for(int indice: e.tab){
         switch(e.tab[indice]){
         case 0:
           if(e.Time_Start > 1 && e.Time_Start < 4.5){
             e.position.x = width*.5 - (e.sizeW*.5);
             e.position.y = -200;
           }
         break;
         case 1:
           if(e.Time_Start > 4.5 && e.Time_Start < 7){
             e.velocity.y = 1.5;
           }
         break;
         case 2:
           if(e.Time_Start > 7 && e.Time_Start < 7.5){     
               e.velocity.y = 0;
           }
         break;
         case 3:
           if(e.Time_Start > 25 && e.Time_Start < 25.5){    
               e.velocity.y = 1.5;
           }
           
         break;
         case 4:
            if(e.Time_Start > 35){
               e.expireNpc = true;
            }
         break;
         }
    }
  }
}

/* Path36l pare dut haut-millieux pour bas-droit */

/* Path60m pare du  haut-droit pour aller bas-gauche */

/* Path63m pare du  haut-droit pour aller bas-milieux */
class Path63m extends Path {
  int[] tab;
  Path63m(int[] patherne){
    this.tab = patherne; 
  }
  void Trigger(Non_Playing_Characte e){
     for(int indice: e.tab){
         switch(e.tab[indice]){
         case 0:
           if(e.Time_Start > 1 && e.Time_Start < 4.5){
             e.position.x = width-100;
             e.position.y = -80;
           }
         break;
         case 1:
           if(e.Time_Start > 4.5 && e.Time_Start < 5){
               e.velocity.y = 3;
           }
         break;
         case 2:
           if(e.Time_Start > 5 && e.Time_Start < 7){     
               e.velocity.y = 0;
           }
         break;
         case 3:
           if(e.Time_Start > 7 && e.Time_Start < 10){    
               e.velocity.y = 4;
               e.velocity.x = -3;
           }
           
         break;
         case 4:
            if(e.Time_Start > 15){
              e.expireNpc = true;
            }
         break;
         }
    }
  }
}

/* Path44l aller de haut-milieux+ pour aller bas-millieux+ */
class Path44l extends Path {
  int[] tab;
  Path44l(int[] Patherne){
     this.tab=Patherne;
  }
  void Trigger(Non_Playing_Characte e){
     for(int indice: e.tab){
         switch(e.tab[indice]){
         case 0:
           if(e.Time_Start > 1 && e.Time_Start < 4.5){
             e.position.x = width-width*.4;
             e.position.y = -80;
           }
         break;
         case 1:
           if(e.Time_Start > 4.5 && e.Time_Start < 5){
               e.velocity.y = 3;
           }
         break;
         case 2:
           if(e.Time_Start > 5 && e.Time_Start < 7){     
               e.velocity.y = 0;
           }
         break;
         case 3:
           if(e.Time_Start > 20 && e.Time_Start < 20.5){    
                e.velocity.y = 1.5;
           }
           
         break;
         case 4:
            if(e.Time_Start > 35){
              e.expireNpc = true;
            }
         break;
         }
    }
  }
}
/* Path22l aller de haut-milieux- pour aller bas-millieux- */
class Path22l extends Path {
  int[] tab;
  Path22l(int[] Patherne){
     this.tab=Patherne;
  }
  void Trigger(Non_Playing_Characte e){
     for(int indice: e.tab){
         switch(e.tab[indice]){
         case 0:
           if(e.Time_Start > 1 && e.Time_Start < 4.5){
             e.position.x = width-width*.66;
             e.position.y = -80;
           }
         break;
         case 1:
           if(e.Time_Start > 4.5 && e.Time_Start < 5){
               e.velocity.y = 3;
           }
         break;
         case 2:
           if(e.Time_Start > 5 && e.Time_Start < 7){     
               e.velocity.y = 0;
           }
         break;
         case 3:
           if(e.Time_Start > 20 && e.Time_Start < 20.5){    
                e.velocity.y = 1.5;
           }
           
         break;
         case 4:
            if(e.Time_Start > 35){
              e.expireNpc = true;
            }
         break;
         }
    }
  }
}
/*
class Patherne {
   int[] tab;
   float r = 4;float thetha = .1;
   
   Patherne(int[] Patherne){
     this.tab=Patherne;
   }
   void Trigger(Non_Playing_Characte e){
       for(int indice: e.tab){
         switch(e.tab[indice]){
           case 0:
               if(e.Time_Start > 2 && e.Time_Start < 3){
                 e.velocity.y=5.14;
               } 
           break;
           case 1:
              if(e.Time_Start > 3 && e.Time_Start < 5){
                 e.velocity.y=0;
                 e.velocity.x=0;
               }
           break;
           case 2:
               if(e.Time_Start > 5){
                 e.velocity.y = r * cos(thetha);
                 e.velocity.x = r * sin(thetha);
                 thetha+= 0.1;
               }
           break;
           case 3:
             if(e.Time_Start > 4 && e.Time_Start < 5){
               
               }
           break;
           case 4:
             if(e.Time_Start > 5 ){
              
             }
           break;
         }
       }
   }
}
*/
/*
class Patherne2 {
  int[] tab;
  Patherne2(int[] Patherne){
     this.tab=Patherne;
  }
  void Trigger(Non_Playing_Characte e){
     for(int indice: e.tab){
         switch(e.tab[indice]){
         case 0:
           if(e.Time_Start > 1 && e.Time_Start < 4.5){
             e.position.x = 100;
             e.position.y = -80;
           }
         break;
         case 1:
           if(e.Time_Start > 4.5 && e.Time_Start < 5){ 
               e.velocity.y = 3;
           }
         break;
         case 2:
           if(e.Time_Start > 5 && e.Time_Start < 7){     
               e.velocity.y = 0;
           }
         break;
         case 3:
           if(e.Time_Start > 7 && e.Time_Start < 10){    
               e.velocity.y = 4;
               e.velocity.x = 2;
           }
           
         break;
         case 4:
            if(e.Time_Start > 15){
               e.expireNpc = true;
            }
         break;
         }
    }
  }
}
*/
/*
class Patherne3 {
  int[] tab;
  Patherne3(int[] Patherne){
     this.tab=Patherne;
  }
  void Trigger(Non_Playing_Characte e){
     for(int indice: e.tab){
         switch(e.tab[indice]){
         case 0:
           if(e.Time_Start > 1 && e.Time_Start < 4.5){
             e.position.x = width-100;
             e.position.y = -80;
           }
         break;
         case 1:
           if(e.Time_Start > 4.5 && e.Time_Start < 5){
               e.velocity.y = 3;
           }
         break;
         case 2:
           if(e.Time_Start > 5 && e.Time_Start < 7){     
               e.velocity.y = 0;
           }
         break;
         case 3:
           if(e.Time_Start > 7 && e.Time_Start < 10){    
               e.velocity.y = 4;
               e.velocity.x = -3;
           }
           
         break;
         case 4:
            if(e.Time_Start > 15){
              e.expireNpc = true;
            }
         break;
         }
    }
  }
}
*/