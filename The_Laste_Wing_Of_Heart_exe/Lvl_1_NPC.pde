///// Non_Playing_Characte
//////////////////////////////////////////////////////////////////
class Non_Playing_Characte implements Entity {
    PImage shieald =loadImage("Sprite/spriteshealdxcf.png");
    // Movement
    PVector position=new PVector();
    PVector velocity=new PVector();
    // Style NPC
    int sizeW;
    int sizeH;
    PImage sprite; 
    // Pathing
    Path path;
    int[] tab;
    int choicepath;
    
    // Timer
    Timer time_appli=new Timer();
    float Timer_Characte, Time_Start;
    // statu 
    Layer layer_npc;
    boolean expireNpc = false; 
    int explose=0;
    boolean activeted = false;
    PImage explose1= loadImage("Explose_Npc/explose_1.png");PImage explose2= loadImage("Explose_Npc/explose_2.png");
    PImage explose3= loadImage("Explose_Npc/explose_3.png");PImage explose4= loadImage("Explose_Npc/explose_4.png");
    PImage explose5= loadImage("Explose_Npc/explose_5.png");PImage explose6= loadImage("Explose_Npc/explose_6.png");
    PImage explose7= loadImage("Explose_Npc/explose_7.png");
    PImage explose8= loadImage("Explose_Npc/explose_8.png");PImage explose9= loadImage("Explose_Npc/explose_9.png");
    PImage explose10= loadImage("Explose_Npc/explose_10.png");PImage explose11= loadImage("Explose_Npc/explose_11.png");
    PImage explose12= loadImage("Explose_Npc/explose_12.png");PImage explose13= loadImage("Explose_Npc/explose_13.png");
    
    Perso user;
    Shot_Npc_pathe_1 shotNpc;
    Layer tab_bullet_npc  = new Layer();
    BarreShield_User user_shield_game_instance;
    BarreDeVie user_heal_gme_instance;
    
    NPC_BarreShield Npc_barreShield;
    NPC_BarreDeVie Npc_barrehp;
    float waponCdForUser = Jambom.std_bow_v1.cd;
    float timerForWaponUser = 0;
    int index;
    int nbColission = 0;
    Non_Playing_Characte(Perso user_in_game,BarreShield_User user_shield,BarreDeVie user_heals,Layer npc,int[] Patherne,int choicePathe,int sizeW,int sizeH,PImage sprite,float Timer_Characte){
      this.layer_npc=npc;
      this.tab=Patherne;
      this.choicepath=choicePathe;
      this.sizeW=sizeW;
      this.sizeH=sizeH;
      this.sprite=sprite;
      this.Timer_Characte=Timer_Characte;
      
      position.y=-200;
      position.x=width*.5;
      user = user_in_game;
      user_shield_game_instance = user_shield;
      user_heal_gme_instance = user_heals;
      shotNpc = new Shot_Npc_pathe_1(this,user);
      
      
      Charater_NPC_Game_Meta NPC_tosu = new  Charater_NPC_Game_Meta("Std_NPC_type_03",sprite);
      NPC_tosu.TypeOfNpcShip();
      Npc_barreShield = new NPC_BarreShield(NPC_tosu.charater_Shield);
      Npc_barrehp =  new NPC_BarreDeVie (NPC_tosu.charater_HP, 0);
      
      tab_bullet_npc.addEntity(shotNpc);
      
      selecterPather(choicepath,0);
    }
    void draw(){
      VoidTimer();
      
      position.y+=velocity.y;
      position.x+=velocity.x;
      
      if(explose != 0){
        image(sprite,position.x,position.y);
        image(explose1,position.x,position.y);
          explose++;
         if(explose > 5 && explose < 10){explose1 = explose2;}
         if(explose > 10 && explose < 15){explose1 = explose3;}
         if(explose > 15 && explose < 20){explose1 = explose4;}
         if(explose > 20 && explose < 25){explose1 = explose5;}
         if(explose > 25 && explose < 30){explose1 = explose6;}
         if(explose > 30 && explose < 35){explose1 = explose7;}
         if(explose > 35 && explose < 40){explose1 = explose8;}
         if(explose > 40 && explose < 45){explose1 = explose9;}
         if(explose > 45 && explose < 50){explose1 = explose10;}
         if(explose > 50 && explose < 55){explose1 = explose10;}
         if(explose > 55 && explose < 60){explose1 = explose10;}
         if(explose > 60 && explose < 65){explose1 = explose10;}
         if(explose >= 65){
           layer_npc.removeEntity(this);
           explose=0;
         }
         }else{
            image(sprite,position.x,position.y);
            Collide_Shot_Npc_Vs_User(user_shield_game_instance,user_heal_gme_instance);
         }
         
       //timerForWaponUser,waponCdForUser
       timerForWaponUser += 0.05;
       if(timerForWaponUser >= waponCdForUser){
         Shot_Npc_pathe_1 newShot = new Shot_Npc_pathe_1(this,user);
         tab_bullet_npc.addEntity(newShot);
         timerForWaponUser = 0;
       }
        
      tab_bullet_npc.draw();
      
      if(Npc_barreShield.NPC_Active_domage == true){
        image(shieald,position.x,position.y);
      }
      
      path.Trigger(this);
    }
    void VoidTimer(){
      time_appli.getTimer();
      Time_Start = time_appli.timer_In_Game_Seconde-Timer_Characte;
    }
    
    void Collide_Shot_Npc_Vs_User(BarreShield_User shield_user_game_lvl,BarreDeVie heals_user_game_lvl){
       
       Iterator<Entity> ite = tab_bullet_npc.uniter.iterator();
       while(ite.hasNext()){
             Shot_Npc_pathe_1 shot_npc =(Shot_Npc_pathe_1)ite.next();
             float sx = user.position.x;
             float SW = user.sizeW;
             float CXA = sx+(SW/2);
             float sy = user.position.y;
             float SH = user.sizeH;
             float CYA = sy+(SH/2)+5;
             
             float nx = shot_npc.position.x;
             float nW = shot_npc.size; 
             float CXB = nx+(nW/2);
             float ny = shot_npc.position.y;
             float nH = shot_npc.size; 
             float CYB = ny+(nH/2);
             
             // fill(0);
             // ellipse(CXA,CYA,SW+20,SH+20);
              float d2 = (CXA-CXB)*(CXA-CXB) + (CYA-CYB)*(CYA-CYB);
              /* Need Works */float d22 = ( (CXA+10) - (CXB+10)) * ((CXA+10) - (CXB+10)) + ( (CYA+10) - (CYB+10)) * ((CYA+10) - (CYB+10));
              if(d22 > (SW+20)*(nW)){
              }else{
                if(shield_user_game_lvl.shield_user > 0 ){
                    shield_user_game_lvl.take_Domage(Jambom.std_speed_fire_v1.degat);
                    
                    if(shield_user_game_lvl.shield_user <= 0){
                      shield_user_game_lvl.Active_domage = false;
                    }
                    
                    ite.remove();
                    break;
                 }
              }
              
              if(d2 > SW*nW){
              }else{
                if(shield_user_game_lvl.shield_user <= 0){
                  shield_user_game_lvl.regencd = 0;
                  
                  heals_user_game_lvl.index =  0;
                  heals_user_game_lvl.take_Domage(Jambom.std_v1.degat,false);
                  ite.remove();
                  break;
                }
              }
              
              if(position.y > height || position.x > width || position.y < 0 || position.x < 0 ){
                  ite.remove();  
              }
       }
    }
    
    void selecterPather(int i,int init){
      if(init == 0) {
        switch(i){
          case 0:
        break;
          case 1:
              path = new Path03m(tab);
        break;
          case 2:
              path = new Path63m(tab);
        break;
          case 3:
              path = new Path33l(tab);
        break;
          case 4:
              path = new Path44l(tab);
        break;
          case 5:
              path = new Path22l(tab);
        break;
          case 6:
              path =new Path001(tab);
        break;
        }
      }
    }
}
class Shot_Npc_pathe_1 implements Entity{
  PVector velocity=new PVector();
  PVector position=new PVector();
  float derectionX;
  float widthNpc;
  float heightNpc;
  int size=10;
  float id;
  float courpe=0;
  int a=0;
  boolean good_trajecte=false;
  Non_Playing_Characte npc;
  Perso User;
  Shot_Npc_pathe_1(Non_Playing_Characte e,Perso user){
  this.size=size;
  User=user;
  npc=e;
  }
  void draw(){
    bullet_Of_Enemi(npc,User);
  }
  void bullet_Of_Enemi(Non_Playing_Characte npc,Perso user){
    if(npc.position.y > 0){
      widthNpc=npc.sizeW/2;
      heightNpc=npc.sizeH;
      
      if(a == 0){
        position.x=npc.position.x+widthNpc;
        position.y=npc.position.y+heightNpc;
      }else if(good_trajecte == false){
          position = new PVector(position.x,position.y);
          velocity = PVector.sub(user.focus,position);
          velocity.normalize();
          velocity.mult(5);
          
          good_trajecte=true;
      }
       position.x+=velocity.x;
       position.y+=velocity.y;
       
       fill(255,0,0);
       ellipse(position.x,position.y,size,size);
       a = 1;
    }
  }
}