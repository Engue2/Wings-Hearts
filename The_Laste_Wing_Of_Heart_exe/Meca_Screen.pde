abstract class Screen {
  ArrayList<Entity> screen = new ArrayList();
  void draw(){
    for(Entity e: screen) {
      e.draw();
    }  
  }
  abstract void keyTyped();
  abstract void keyPressed();
  abstract void keyReleased();
  abstract void mouseClicked();
  
  void addEntity(Entity e){
    screen.add(e);
  }
  
}
interface Entity {
  void draw();
}

class Layer implements Entity{
  ArrayList<Entity> uniter = new ArrayList();
  ArrayList<Entity> delete = new ArrayList();
  
  void draw(){
    for(Entity e: uniter){
      e.draw();
    }
  }
  void updtate(){
    Iterator<Entity> ite = delete.iterator();
    while(ite.hasNext()){
      Entity uniterDelete = (Entity)ite.next();
      
      uniter.remove(uniterDelete);
      ite.remove();
    }
  }
  void addEntity(Entity e){
    uniter.add(e);
  }
  
  void removeEntity(Entity e){
    delete.add(e);
  } 
}