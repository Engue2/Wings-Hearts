import java.util.Iterator;
import java.util.Date;

float Timer=millis();

/*
Screen serre ici de mécanique d'affichage du Niveau/Interface acutelle. 
*/
Screen currentScreen;
// Voici Mon project Perso Crée est co produit par Enguerrand Meheut .
// Type de Jeux : "Shmup" ou "Jeux de tir"
// On Utilise l'environment processing .
void setup(){
   size(860,570,FX2D);
   //fullScreen(FX2D,SPAN);
   currentScreen = new Start_Game();
   //currentScreen = new Menu();
}
void draw(){
  currentScreen.draw();
}
void keyTyped(){
  currentScreen.keyTyped();
}
void keyPressed(){
  currentScreen.keyPressed();
}
void keyReleased(){
   currentScreen.keyReleased();
}
void mouseClicked(){
   currentScreen.mouseClicked();
}

void nextStage(int id){
    if(id == 1){
    // Ici nous avont le lancement de L'ecran Menu();
    currentScreen = new Menu();
    }
    if(id == 2){
    // Ici nous avont le lancement de L'écran lvl1();
    currentScreen = new lvl1();
    }
}