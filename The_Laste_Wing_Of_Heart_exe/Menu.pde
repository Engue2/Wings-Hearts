class Menu extends Screen{
  PImage SCreenBackGround2 = loadImage("Image/Ship_MenuIng_2.png");
  PImage SCreenBackGround3 = loadImage("Image/terre_018 - Copie.jpg");
  
  PFont myFont;
  PFont myFontLow;
  String textNG="New Game";
  String textSG="Select you'r save";
  String textOption="Option";
  String textLeav="Leav";
  
  String[] lines;
  int numberOfSave = 0;
  boolean lunchNewGame = false;
  String Id_User_Name_File="";
  String Delete="";
  
  boolean activeMenu = true;
  boolean newGame = false; 
  boolean selectYourSave = false;
  boolean optionMenu = false;
  boolean leavGame = false;
  String vraiment="Voulez vous vraiment quitter le jeu ?";
  String yesValid="Oui";
  String noValid="Non";
  Menu (){
  //String[] fontList = PFont.list();
  //printArray(fontList);
  myFontLow = createFont("Corbel Bold", 20);
  myFont = createFont("Corbel Bold", 32);
  textFont(myFont);
  }
  void draw(){
   background(255);
   backGroundScreenMenu();
   
   GameMenuButton();
   Game_Name_Interface_button();
    
   NewGame(); 
   SelectYourSave(); 
   OptionGame();
   LeavGame();
   
   if(lunchNewGame == true){
     // Si une nouvelle partis est lancer !
     currentScreen = new Player_Select_chose(Id_User_Name_File,numberOfSave);
   }
  }
  
  void keyTyped(){
    if(Id_User_Name_File.length() < 12 && newGame == true){
      if ((key >= 'A' && key <= 'z') || key == ' ') {
        Id_User_Name_File = Id_User_Name_File + key;
      }
    }
  }
  void keyPressed(){  
     if(keyCode == 8){
      for(int i=0;i<Id_User_Name_File.length()-1;i++){
         Delete+=Id_User_Name_File.charAt(i);
      }
      Id_User_Name_File=Delete;
      Delete="";
    }
  }
  void keyReleased(){}
  void LeavGame(){
     if(leavGame == false){
     }else{
      fill(160, 108, 108);
      rectMode(CENTER);
      rect(width/2,height/2,width*.25,height*.25); // rect validation
      rect(width/2-width*.06,height/2,width*.08,height*.08); // yes
      rect(width/2+width*.06,height/2,width*.08,height*.08); // non
      fill(255);
      text(vraiment,width*.30,height*.30);
      fill(255,0,0);
      text(yesValid,width*.42,height*.52); 
      text(noValid,width*.54,height*.52);
      textFont(myFont);
      rectMode(CORNER);
    }
  }
  
  void mouseClicked(){
    if(newGame == true){
      if(mouseX > 105 && mouseX < 215 && mouseY > 190 && mouseY < 240){
        if(Id_User_Name_File.length() != 0){
            // Start- certe a ajouter le nom du joueur au tant que nouvelle sauvegarde si il a plus de 3 sauvegarde il ne se passe rien.
            lines =  loadStrings("user.txt");
            if(numberOfSave < lines.length){
              String[] tabName = split(lines[numberOfSave],"-");
              for(int i=0; i<tabName.length;i++){
                numberOfSave++;
              }
              
              if(numberOfSave > 100){
                println("Vous avez plus de 3 sauvegarde");
              }else{
                lines[0] = lines[0]+"-"+Id_User_Name_File;
                saveStrings("user.txt", lines);
                lunchNewGame = true;
              }
            }
        }else {
          //  message d'errore (pas de nom donner a la sauvegarde.)
        }
      }
      /* 
      newGame
      rect(105,190,110,50);
      */
    }
    if(activeMenu == true){
       // New Game
       if(mouseX > width*.73 && mouseX < width*.885 && mouseY > height*.3 && mouseY < height*.36){
         //println("New Game");
         newGame = true;
         activeMenu = false;
       }
       
       if(mouseX > width*.73 && mouseX < width*.885 && mouseY > height*.45 && mouseY < height*.51){
         // Save Game
         println("Selecte You'r Save");
         currentScreen = new lvl1();
         selectYourSave = true;
         activeMenu = false;
       }
       
       if(mouseX > width*.73 && mouseX < width*.885 && mouseY > height*.60 && mouseY < height*.66){
         // Option
         println("Option");
         optionMenu = true;
         activeMenu = false;
       }
       
        if(mouseX > width*.73 && mouseX < width*.885 && mouseY > height*.75 && mouseY < height*.81){
          // Leav
          println("Leav");
          leavGame = true;
          activeMenu = false;
       }
    }else{
     
      if(mouseX > width*.5 && mouseX < width*.515 && mouseY < height*.03){
         newGame = false;
         selectYourSave = false;
         optionMenu = false;
         activeMenu = true;
      }
      // leavGame :: 
      if(mouseX > width*.40 && mouseX < width*.48 && mouseY > height*.45 && mouseY < height*.54){ exit(); }
      if(mouseX > width*.52 && mouseX < width*.60 && mouseY > height*.45 && mouseY < height*.54){
         leavGame = false;
         activeMenu = true;
      }
    }
  }
   void backGroundScreenMenu(){
    image(SCreenBackGround3,0,-height/4);
    image(SCreenBackGround2,0,height*.43);
   }
   
   void GameMenuButton(){
   fill(13, 39, 158);
   rect(width*.73,height*.3,200,40); // New Game
   
   rect(width*.73,height*.45,200,40); // Save Game
   
   rect(width*.73,height*.60,200,40); // Option
   
   rect(width*.73,height*.75,200,40); // Leav
   }
   void Game_Name_Interface_button(){
    fill(0);
    text(textNG, width*.73,height*.35); // new Game
    
    text(textSG, width*.73,height*.50); // Save Game
    
    text(textOption, width*.73,height*.65); // Option
    
    text(textLeav, width*.73,height*.80); // Leav
   }
   void NewGame(){
     if(newGame == false){
      }else{
      fill(255);
      rect(20,20,width/2,height/2);
    
      fill(255,0,0);
      rect(width/2,0,20,20);
      
      fill(0);
      text("Entre le nom du joueur",50,55);
      text(Id_User_Name_File,50,120);
      
      // Button Valider
      fill(255);
      stroke(0);
      rect(105,190,110,50);
      fill(0);
      text("Valider",110,225);
      // Annuler
      fill(255);
      stroke(0);
      rect(255,190,110,50);
      fill(0);
      text("Annuler",255,225);
      
      fill(0);
      line(width*.5,0,width*.515,20);
      line(width*.515,0,width*.5,20);
      }
   }
   void SelectYourSave(){
      if(selectYourSave == false){
      }else{
      nextStage(2);
      fill(255);
      rect(20,20,width/2,height/2);
    
      fill(255,0,0);
      rect(width/2,0,20,20);
    
      fill(0);
      line(width*.5,0,width*.515,20);
      line(width*.515,0,width*.5,20);
      }
   }
   void OptionGame(){
     if(optionMenu == false){
      }else{
      fill(255);
      rect(20,20,width/2,height/2);
    
      fill(255,0,0);
      rect(width/2,0,20,20);
    
      fill(0);
      line(width*.5,0,width*.515,20);
      line(width*.515,0,width*.5,20);
      }
   }
}

class Start_Game extends Screen{
  PImage Backgroudn2 = loadImage("Image/terre_018 - Copie.jpg");
  PFont myfont;
  PFont myfont1;
  String TitelGame_1 = "Wing's of";
  String TitelGame_2 = "Hearts";
  String start_blink = "Appuyer pour passer.";
  int a=245;
  boolean rotate=false;
  
  Start_Game(){
  myfont = createFont("Corbel Bold", 32);
  myfont1 = createFont("Corbel Bold", 65);
  }
  
  void draw(){
    background(255);
    backgroundImage();
    titelScreenAttemps();
    pressedKey();  
  }
  void keyTyped(){}
  void keyPressed(){
    nextStage(1);
  }
  void keyReleased(){}
  void mouseClicked(){}
  
  void titelScreenAttemps(){
    fill(255);
    textFont(myfont1);
    text(TitelGame_1,width*.40,height*.3);
    text(TitelGame_2,width*.43,height*.45);
  }
  
  void backgroundImage(){
    image(Backgroudn2,0,-100);
  }
  
  void pressedKey(){
   if(a <= 245 && rotate == false){
     a+=-2;
       if(a < 30){ rotate = true;}
   }
   if(a <= 245 && rotate == true){
     a+=2;
       if(a >= 245){rotate = false;}
   } 
    tint(245, 245);
    fill(a);
    textFont(myfont);
    text(start_blink,width*.39,height*.85);
    noTint();
  }
}