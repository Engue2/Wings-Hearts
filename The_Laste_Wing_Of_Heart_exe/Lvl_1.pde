class lvl1 extends Screen {
  PImage a = loadImage("Sprite/Sprite_Npc_1_littel.png"); // Sprite_Npc_1_littel.png 50 80
  PImage A = loadImage("Sprite/Sprite_Npc_1.png"); // Sprite_Npc_1.png 110 160 
  PImage b = loadImage("Sprite/Sprite_Npc_2.png"); // Sprite_Npc_2.png  69 65 
  PImage shieald =loadImage("Sprite/spriteshealdxcf.png");
  int index=0;
  // backGround_In_Game
  Layer tabStar;

  // NPC
  Layer tabNpc;
  
  // User
  Charater_Game_Meta tosu;
  PImage sprite;
  Perso user;
  Layer tabShot;

  BarreDeVie barrehp;
  BarreShield_User barreshield;
  
  float waponCdForUser = Jambom.std_v1.cd;
  float timerForWaponUser = 0;
  lvl1 () {
    // backGround_In_Game
    tabStar = new Layer();
    for (int i=0; i<20; i++) {
      Star etoile=new Star();
      tabStar.addEntity(etoile);
    }  
    addEntity(tabStar);
    
    // User  Type : Type_1 ,Soldat , Ghost , Ingé :
    tosu = new Charater_Game_Meta("Type_1", sprite);
    tosu.TypeOfShip();

    user = new Perso(tosu.ship_disigne, tosu.charater_Size, tosu.charater_Size);
    tabShot = new Layer();
    
    barreshield = new BarreShield_User(tosu.charater_Shield,tosu.charater_Regen_S,tosu.charater_Speed_Regen_S);
    barrehp = new BarreDeVie(tosu.charater_HP, 0);

    addEntity(tabShot);
    addEntity(user);
    
    // NPC
    tabNpc = new Layer();
    
    int[] tabMainNpcWaveT = {0,1,2,3};
    
    Non_Playing_Characte azerty = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWaveT,6,110,160,A,0); 
    tabNpc.addEntity(azerty);
    
    
    // 1 eme vague .
    float wave_1_Right=0;
    int[] tabMainNpcWave = {0,1,2,3,4};
    wave_1_Right = wave_1_Right + 0;
    
    Non_Playing_Characte npc_Large=new Non_Playing_Characte(user,barreshield,barrehp,tabNpc, tabMainNpcWave,3, 110, 160, A, wave_1_Right);
    tabNpc.addEntity(npc_Large);
    wave_1_Right = wave_1_Right + 3;
    Non_Playing_Characte npc_first_Wave_low_Right_1 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,4,50,80,a,wave_1_Right);
    tabNpc.addEntity(npc_first_Wave_low_Right_1);
    Non_Playing_Characte npc_first_Wave_low_left_1 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,5,50,80,a,wave_1_Right);
    tabNpc.addEntity(npc_first_Wave_low_left_1);
    for(int i=0;i<4;i++){
      Non_Playing_Characte npc_first_Wave1 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,2,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave1);
      
      Non_Playing_Characte npc_first_Wave2 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,1,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave2); 
      
      wave_1_Right = wave_1_Right + 4;
    }
    
    // 2 eme vague .
    wave_1_Right = wave_1_Right + 10;
    Non_Playing_Characte npc_Large_2 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc, tabMainNpcWave,3, 110, 160, A, wave_1_Right);
    tabNpc.addEntity(npc_Large_2);
    wave_1_Right = wave_1_Right + 3;
    for(int i=0;i<4;i++){
      Non_Playing_Characte npc_first_Wave1 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,2,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave1);
      
      Non_Playing_Characte npc_first_Wave2 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,1,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave2);
      
      wave_1_Right = wave_1_Right + 4;
    }
    
    
    // 3 eme vague .
    wave_1_Right = wave_1_Right + 10;
    Non_Playing_Characte npc_Large_3 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc, tabMainNpcWave,3, 110, 160, A, wave_1_Right);
    tabNpc.addEntity(npc_Large_3);
    wave_1_Right = wave_1_Right + 3;
    for(int i=0;i<4;i++){
      Non_Playing_Characte npc_first_Wave1 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,2,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave1);
      
      Non_Playing_Characte npc_first_Wave2 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,1,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave2);
      
      wave_1_Right = wave_1_Right + 4;
    }
    
    // 4 eme vague .
    wave_1_Right = wave_1_Right + 5;
    Non_Playing_Characte npc_Large_5=new Non_Playing_Characte(user,barreshield,barrehp,tabNpc, tabMainNpcWave,3, 110, 160, A, wave_1_Right);
    tabNpc.addEntity(npc_Large_5);
    wave_1_Right = wave_1_Right + 10;
    Non_Playing_Characte npc_first_Wave_low_Right_4 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,4,50,80,a,wave_1_Right);
    tabNpc.addEntity(npc_first_Wave_low_Right_4);
    Non_Playing_Characte npc_first_Wave_low_left_4 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,5,50,80,a,wave_1_Right);
    tabNpc.addEntity(npc_first_Wave_low_left_4);
    for(int i=0;i<4;i++){
      Non_Playing_Characte npc_first_Wave1 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,2,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave1);
      
      Non_Playing_Characte npc_first_Wave2 = new Non_Playing_Characte(user,barreshield,barrehp,tabNpc,tabMainNpcWave,1,50,80,a,wave_1_Right);
      tabNpc.addEntity(npc_first_Wave2); 
      
      wave_1_Right = wave_1_Right + 4;
    }
    
    
    addEntity(tabNpc);
  }
  void draw() {
    background(0);
    barrehp.draw();
    barreshield.draw();
    
    super.draw();
    
    tabShot.updtate();
    tabNpc.updtate();
  
    //User Wapon rate
    timerForWaponUser += 0.05;
    if(timerForWaponUser >= waponCdForUser){
      Shot_User_In_Game Shot_User=new Shot_User_In_Game(sprite, 10, 10, tabShot, user.position.x, user.position.y, 5);
      tabShot.addEntity(Shot_User);
      timerForWaponUser = 0;
    }
    
    Shot_UserVsNpc();
    User_Vs_Npc();
    
    if(barreshield.Active_domage == true){
      image(shieald,user.position.x-(user.sizeW/4.3),user.position.y-(user.sizeH/4.3));
    }
  }
  void keyTyped(){}
  void keyPressed() {
    user.openType(tosu.charater_Speed);
  }
  void keyReleased() {
    user.endType();
  }
  void mouseClicked() {
  }
  
  void Shot_UserVsNpc() {
    Iterator<Entity> iteShot = tabShot.uniter.iterator();
    while (iteShot.hasNext()) {
      Shot_User_In_Game shot = (Shot_User_In_Game)iteShot.next();
      float sx = shot.position.x;
      float sy = shot.position.y;

      Iterator<Entity> ite = tabNpc.uniter.iterator();
      while (ite.hasNext()) {
        Non_Playing_Characte npc = (Non_Playing_Characte)ite.next();
        if(sy < 0){
          iteShot.remove();
          break;
        }
        
        if(sx > npc.position.x - 10 && sx < npc.position.x + npc.sizeW + 10 && 
           sy > npc.position.y - 10 && sy < npc.position.y + npc.sizeH + 10 && npc.Npc_barreShield.NPC_shield > 0){
           
             float RellyDomage = Jambom.std_v1.degat + (Jambom.std_v1.BonusDomageWhitShield * (Jambom.std_v1.degat / 100));
             npc.Npc_barreShield.take_Domage(RellyDomage);
             
             if(npc.Npc_barreShield.NPC_shield <= 0){
               npc.Npc_barreShield.NPC_Active_domage = false;
             }
          iteShot.remove();
          break;
        }
        
        if (sx > npc.position.x && sx < npc.position.x + npc.sizeW &&
            sy > npc.position.y && sy < npc.position.y + npc.sizeH && npc.activeted == false) {
             float RellyDomage = Jambom.std_v1.degat + (Jambom.std_v1.BonusDimagWhitePv * (Jambom.std_v1.degat / 100));
             npc.Npc_barrehp.take_Domage(RellyDomage,false);
             if(npc.Npc_barrehp.NPC_hp <= 0){
               npc.activeted = true;
               npc.explose = 1;
             }
          
          iteShot.remove();
          break;
        }
      }
    }
  }
  void User_Vs_Npc() {
    float sx = user.position.x;
    float SW = user.sizeW;
    float CXA = sx+(SW/2);
    float sy = user.position.y;
    float SH = user.sizeH;
    float CYA = sy+(SH/2)+5;    
    //     ellipseMode(CENTER);
    //     fill(255,0,0);
    //     ellipse(sx+(SW/2),sy+(SH/2)+5,SW,SW); 
    Iterator<Entity> ite = tabNpc.uniter.iterator();
    while (ite.hasNext()) {
      Non_Playing_Characte npc = (Non_Playing_Characte)ite.next();
      
      float nx = npc.position.x;
      float nW = npc.sizeW; 
      float CXB = nx+(nW/2);
      float ny = npc.position.y;
      float nH = npc.sizeH; 
      float CYB = ny+(nH/2);
      //   fill(229,0,0);
      //   ellipse(nx+(npc.sizeW/2),ny+(npc.sizeH/2),npc.sizeW,npc.sizeW);
      float d2 = (CXA-CXB)*(CXA-CXB) + (CYA-CYB)*(CYA-CYB);
      if (d2 > SW*nW) {
      } else {
        user.val = 200;
        barrehp.take_Domage(10, barrehp.Active_domage);
        break;
      }
      if(npc.expireNpc == true){
         ite.remove();
         break;
      }
    }
  }
}